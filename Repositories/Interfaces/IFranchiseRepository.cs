﻿using Movie_Characters_API.Model.Domain;

namespace Movie_Characters_API.Repositories.Interfaces
{
    public interface IFranchiseRepository
    {
        /// <summary>
        /// Get all franchises from the database.
        /// </summary>
        /// <returns>A list of franchises.</returns>
        public Task<List<Franchise>> GetAllFranchises();

        /// <summary>
        /// Get a franchise with given <paramref name="franchiseId"/> from database.
        /// </summary>
        /// <param name="franchiseId">The id of a franchise.</param>
        /// <returns>A franchise object.</returns>
        public Task<Franchise> GetFranchise(int franchiseId);

        /// <summary>
        /// Get all movies related to a franchise with given <paramref name="franchiseId"/> from the database.
        /// </summary>
        /// <param name="franchiseId">The id of a franchise.</param>
        /// <returns>A franchise Object.</returns>
        public Task<Franchise> GetAllMoviesInFranchise(int franchiseId);

        /// <summary>
        /// Get all characters in a franchise with given <paramref name="franchiseId"/> from database.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <returns>A list of characters.</returns>
        public Task<List<Character>> GetCharactersInFranchise(int franchiseId);

        /// <summary>
        /// Update a franchise in database.
        /// </summary>
        /// <param name="franchise">The given franchise object.</param>
        /// <returns>True if successful update. Else false.</returns>
        public Task<bool> UpdateFranchise(Franchise franchise);

        /// <summary>
        /// Check if a franchise with the given <paramref name="franchiseId"/> exists in database.
        /// </summary>
        /// <param name="franchiseId">The id of a franchise.</param>
        /// <returns>True if franchise exists. Else false.</returns>
        public Task<bool> FranchiseExist(int franchiseId);

        /// <summary>
        /// Create a franchise in database.
        /// </summary>
        /// <param name="franchise">The given franchise object.</param>
        /// <returns>The franchise that was added to the database.</returns>
        public Task<Franchise> CreateFranchise(Franchise franchise);

        /// <summary>
        /// Delete a franchise with given <paramref name="franchiseId"/> from the database.
        /// </summary>
        /// <param name="franchiseId">The id of a franchise.</param>
        /// <returns>The franchise that was deleted from database.</returns>
        public Task<Franchise> DeleteFranchise(int franchiseId);

        /// <summary>
        /// Check if the database contains a entity set for franchises.
        /// </summary>
        /// <returns>True if franchise entity exists in database. Else false.</returns>
        public bool FranchiseEntitySetExist();
    }
}
