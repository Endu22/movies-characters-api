using Movie_Characters_API.Model.Domain;

namespace Movie_Characters_API.Repositories.Interfaces
{
    public interface ICharacterRepository
    {
        /// <summary>
        /// Get all characters from the database.
        /// </summary>
        /// <returns>A list of characters.</returns>
        public Task<List<Character>> GetAllCharacters();
        
        /// <summary>
        /// Get a character specified by it's <paramref name="characterId"/> from the database.
        /// </summary>
        /// <param name="characterId">The id of a character.</param>
        /// <returns>A character.</returns>
        public Task<Character> GetCharacter(int characterId);


        /// <summary>
        /// Update a character in the database.
        /// </summary>
        /// <param name="character">A character object.</param>
        /// <returns>True if sucessful. Else false.</returns>
        public Task<bool> UpdateCharacter(Character character);

        /// <summary>
        /// Check if a character with given <paramref name="characterId"/> exists in the database.
        /// </summary>
        /// <param name="characterId">The id of a character.</param>
        /// <returns>True if character exists. Else false.</returns>
        public Task<bool> CharacterExists(int characterId);

        /// <summary>
        /// Creates given <paramref name="character"/> in database.
        /// </summary>
        /// <param name="character">The character object to be added to the database.</param>
        /// <returns>The object that was inserted.</returns>
        public Task<Character> CreateCharacter(Character character);

        /// <summary>
        /// Check if the database contains the entity set for characters.
        /// </summary>
        /// <returns>True if the entity set exists. Else false.</returns>
        public bool CharactersEntitySetExist();

        /// <summary>
        /// Delete a character with given <paramref name="characterId"/> from the database.
        /// </summary>
        /// <param name="characterId">The id of a character.</param>
        /// <returns>The deleted character.</returns>
        public Task<Character> DeleteCharacter(int characterId);
    }
}
