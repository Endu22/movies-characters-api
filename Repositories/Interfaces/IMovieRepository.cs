﻿using Movie_Characters_API.Model.Domain;

namespace Movie_Characters_API.Repositories.Interfaces
{
    public interface IMovieRepository
    {
        /// <summary>
        /// Get all movies from the database.
        /// </summary>
        /// <returns>A list of movies.</returns>
        public Task<List<Movie>> GetAllMovies();

        /// <summary>
        /// Get a movie specified by it's <paramref name="movieId"/> from the database.
        /// </summary>
        /// <param name="movieId">The id of a movie.</param>
        /// <returns>A movie</returns>
        public Task<Movie> GetMovie(int movieId);

        /// <summary>
        /// Get a movie specified by it's <paramref name="movieId"/>, with all the characters it has from the database.
        /// </summary>
        /// <param name="movieId">The movie id.</param>
        /// <returns>A movie object.</returns>
        public Task<Movie> GetMovieWithCharacters(int movieId);

        /// <summary>
        /// Get all the characters in a movies specified by it's <paramref name="movieId"/>.
        /// </summary>
        /// <param name="movieId">The id of a movie.</param>
        /// <returns>A movie object.</returns>
        public Task<Movie> GetAllCharactersInMovie(int movieId);

        /// <summary>
        /// Update a movie in database.
        /// </summary>
        /// <param name="movie">The movie to update.</param>
        /// <returns>True if successful. Else false.</returns>
        public Task<bool> UpdateMovie(Movie movie);

        /// <summary>
        /// Add a movie in database.
        /// </summary>
        /// <param name="movie">The movie to add.</param>
        /// <returns>The movie added.</returns>
        public Task<Movie> CreateMovie(Movie movie);

        /// <summary>
        /// Delete a movie with <paramref name="movieId"/> from database.
        /// </summary>
        /// <param name="movieId">The id of a movie.</param>
        /// <returns>The movie deleted from database.</returns>
        public Task<Movie> DeleteMovie(int movieId);

        /// <summary>
        /// Save the context changes in database.
        /// </summary>
        /// <returns>True if successful. Else false.</returns>
        public Task<bool> SaveChanges();

        /// <summary>
        /// Check if a movie with <paramref name="movieId"/> exists in database.
        /// </summary>
        /// <param name="movieId">The id of a movie.</param>
        /// <returns>True if a movie with given <paramref name="movieId"/> eists. Else false.</returns>
        public Task<bool> MovieExists(int movieId);

        /// <summary>
        /// Check if the context contains a set for movies.
        /// </summary>
        /// <returns>true if context contains a set for movies. Else false.</returns>
        public bool MovieEntitySetExist();
    }
}
