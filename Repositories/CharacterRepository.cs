using Microsoft.EntityFrameworkCore;
using Movie_Characters_API.Data;
using Movie_Characters_API.Model.Domain;
using Movie_Characters_API.Model.DTOs.Character;
using Movie_Characters_API.Repositories.Interfaces;

namespace Movie_Characters_API.Repositories
{
    public class CharacterRepository : ICharacterRepository
    {
        private readonly MoviesCharactersApiDbContext _context;

        public CharacterRepository(MoviesCharactersApiDbContext context)
        {
            _context = context;
        }

        public async Task<List<Character>> GetAllCharacters()
        {
            if (_context.Characters == null)
                throw new Exception("No Characters in Database");

            return await _context.Characters.Include(m => m.Movies).ToListAsync();
        }

        public async Task<Character> GetCharacter(int characterId)
        {
            if (_context.Characters == null)
                throw new Exception("No Characters in Database");

            return await _context.Characters.Include(c => c.Movies).FirstOrDefaultAsync(c => c.Id == characterId);
        }

        public async Task<bool> UpdateCharacter(Character character)
        {
            _context.Entry(character).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return true;
        }

        public async Task<Character> CreateCharacter(Character character)
        {
            await _context.Characters.AddAsync(character);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }
            return character;
        }

        public async Task<bool> CharacterExists(int characterId)
        {
            return await _context.Characters.AnyAsync(c => c.Id == characterId);
        }

        public bool CharactersEntitySetExist()
        {
            if (_context.Characters == null)
                return false;

            return true;
        }

        public async Task<Character> DeleteCharacter(int characterId)
        {
            var character = await _context.Characters.FindAsync(characterId);

            if (character != null)
            {
                _context.Characters.Remove(character);
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
            }
            return character;
        }
    }
}
