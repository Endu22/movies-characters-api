﻿using Microsoft.EntityFrameworkCore;
using Movie_Characters_API.Data;
using Movie_Characters_API.Model.Domain;
using Movie_Characters_API.Repositories.Interfaces;

namespace Movie_Characters_API.Repositories
{
    public class MovieRepository : IMovieRepository
    {
        private readonly MoviesCharactersApiDbContext _context;

        public MovieRepository(MoviesCharactersApiDbContext context)
        {
            _context = context;
        }

        public async Task<List<Movie>> GetAllMovies()
        {
            if (_context.Movies == null)
                throw new Exception("No Movies in Database");

            return await _context.Movies.Include(m => m.Characters).Include(m => m.Franchise).ToListAsync();
        }

        public async Task<Movie> GetMovie(int movieId)
        {
            if (_context.Movies == null)
                throw new Exception("No Characters in Database");

            return await _context.Movies.Include(m => m.Characters).Include(m => m.Franchise).Where(m => m.Id == movieId).FirstOrDefaultAsync();
        }

        public async Task<Movie> GetMovieWithCharacters(int movieId)
        {
            return await _context.Movies
                .Include(m => m.Characters)
                .Where(m => m.Id == movieId)
                .FirstAsync();
        }

        public async Task<Movie> GetAllCharactersInMovie(int movieId)
        {
            return await _context.Movies
                .Include(m => m.Characters)
                .Where(f => f.Id == movieId)
                .FirstAsync();
        }

        public async Task<bool> UpdateMovie(Movie movie)
        {
            _context.Entry(movie).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            return true;
        }

        public async Task<Movie> CreateMovie(Movie movie)
        {
            await _context.Movies.AddAsync(movie);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            return movie;
        }

        public async Task<Movie> DeleteMovie(int movieId)
        {
            var movie = await _context.Movies.FindAsync(movieId);

            if (movie != null)
            {
                _context.Movies.Remove(movie);
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
            }
            return movie;
        }

        public async Task<bool> SaveChanges() {
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            return true;
        }

        public bool MovieEntitySetExist()
        {
            if (_context.Movies == null)
                return false;
            return true;
        }

        public async Task<bool> MovieExists(int movieId)
        {
            return await _context.Movies.AnyAsync(m => m.Id == movieId);
        }
    }
}
