﻿using Microsoft.EntityFrameworkCore;
using Movie_Characters_API.Data;
using Movie_Characters_API.Model.Domain;
using Movie_Characters_API.Repositories.Interfaces;

namespace Movie_Characters_API.Repositories
{
    public class FranchiseRepository : IFranchiseRepository
    {
        private readonly MoviesCharactersApiDbContext _context;

        public FranchiseRepository(MoviesCharactersApiDbContext context)
        {
            _context = context;
        }

        public async Task<List<Franchise>> GetAllFranchises()
        {
            if (_context.Franchises == null)
                throw new Exception("No Franchises in Database");
            return await _context.Franchises.Include(m => m.Movies).ToListAsync();
        }

        public async Task<Franchise> GetFranchise(int franchiseId)
        {
            if(_context.Franchises == null)
                throw new Exception("No Franchises in Database");
            return await _context.Franchises.Include(f => f.Movies).FirstOrDefaultAsync(f => f.Id == franchiseId);
        }

        public async Task<Franchise> GetAllMoviesInFranchise(int franchiseId)
        {
            return await _context.Franchises.Include(m => m.Movies).Where(f => f.Id == franchiseId).FirstAsync();
        }

        public async Task<List<Character>> GetCharactersInFranchise(int franchiseId)
        {
           return await _context.Movies.Where(m => m.FranchiseId == franchiseId).SelectMany(m => m.Characters).Distinct().ToListAsync();
        }
        public async Task<Franchise> CreateFranchise(Franchise franchise)
        {
            await _context.Franchises.AddAsync(franchise);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw;
            }
            return franchise;
        }

        public async Task<Franchise> DeleteFranchise(int franchiseId)
        {
            var franchise = await _context.Franchises.FindAsync(franchiseId);
            _context.Remove(franchise);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {

                throw;
            }

            return franchise;
        }

        public bool FranchiseEntitySetExist()
        {
            if (_context.Franchises == null)
                return false;

            return true;
        }

        public async Task<bool> FranchiseExist(int franchiseId)
        {
            return await _context.Franchises.AnyAsync(f => f.Id == franchiseId); 
        }

        public async Task<bool> UpdateFranchise(Franchise franchise)
        {
            _context.Entry(franchise).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return true;
        }
    }
}
