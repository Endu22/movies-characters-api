using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using Movie_Characters_API.Data;
using Movie_Characters_API.Repositories;
using Movie_Characters_API.Repositories.Interfaces;
using Movie_Characters_API.Services;
using Movie_Characters_API.Services.Interfaces;
using System.Reflection;
using System.Text.Json.Serialization;

namespace Movie_Characters_API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Movie Character API",
                    Version = "v1",
                    Description = "This is the API for managing Movie Character API",
                    Contact = new OpenApiContact
                    {
                        Name = "Oliver Rimmi\n Elmin Nurkic",
                        Email = "oliver.rimmi@se.experis.com\n elmin.nurkic@se.experis.com",
                    },
                    License = new OpenApiLicense
                    {
                        Name = "Use under MIT",
                        Url = new Uri("https://opensource.org/licenses/MIT"),
                    }
                });
                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
          
            builder.Services.AddDbContext<MoviesCharactersApiDbContext>(options =>
                options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));

           builder.Services.AddControllers().AddJsonOptions(x =>
            x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);

            builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            // Inject dependencies
            builder.Services.AddScoped<ICharacterRepository, CharacterRepository>();
            builder.Services.AddScoped<ICharacterService, CharacterService>();
            builder.Services.AddScoped<IFranchiseRepository, FranchiseRepository>();
            builder.Services.AddScoped<IFranchiseService, FranchiseService>();

            builder.Services.AddScoped<IMovieRepository, MovieRepository>();
            builder.Services.AddScoped<IMovieService, MovieService>();

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }
}