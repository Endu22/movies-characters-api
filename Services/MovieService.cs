﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Movie_Characters_API.Model.Domain;
using Movie_Characters_API.Model.DTOs.CharacterMovie;
using Movie_Characters_API.Model.DTOs.Movie;
using Movie_Characters_API.Repositories.Interfaces;
using Movie_Characters_API.Services.Interfaces;

namespace Movie_Characters_API.Services
{
    public class MovieService : IMovieService
    {
        private readonly IMovieRepository _movieRepository;
        private readonly ICharacterRepository _characterRepository;
        private readonly IMapper _mapper;

        public MovieService(IMovieRepository movieRepository, ICharacterRepository characterRepository, IMapper mapper)
        {
            _movieRepository = movieRepository;
            _characterRepository = characterRepository;
            _mapper = mapper;
        }

        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetAllMovies()
        {
            return _mapper.Map<List<MovieReadDTO>>(await _movieRepository.GetAllMovies());
        }

        public async Task<ActionResult<MovieReadDTO>> GetMovie(int movieId)
        {
           return _mapper.Map<MovieReadDTO>(await _movieRepository.GetMovie(movieId));
        }

        public async Task<ActionResult<CharacterMovieReadDTO>> GetAllCharactersInMovie(int characterId)
        {
            return _mapper.Map<CharacterMovieReadDTO>(await _movieRepository.GetAllCharactersInMovie(characterId));
        }

        public async Task<bool> UpdateMovie(int movieId, MovieEditDTO dtoMovie)
        {
            var movie = await _movieRepository.GetMovie(movieId);

            _mapper.Map(dtoMovie, movie);
            return await _movieRepository.UpdateMovie(movie);
        }

        public async Task<bool> UpdateCharactersInMovie(int movieId, int[] characterIds)
        {
            Movie movieToUpdateCharacters = await _movieRepository.GetMovieWithCharacters(movieId);

            List<Character> characters = new();
            foreach (int charId in characterIds)
            {
                Character? domainCharacter = await _characterRepository.GetCharacter(charId);

                if (domainCharacter == null)
                    throw new KeyNotFoundException();

                characters.Add(domainCharacter);
            }

            movieToUpdateCharacters.Characters = characters;
            var didSave = await _movieRepository.SaveChanges();
            
            if (didSave)
                return true;

            return false;
        }

        public async Task<Movie> CreateMovie(MovieCreateDTO movieCreateDTO)
        {
            var movie = _mapper.Map<Movie>(movieCreateDTO);
            return await _movieRepository.CreateMovie(movie);
        }

        public async Task<Movie> DeleteMovie(int movieId)
        {
            return await _movieRepository.DeleteMovie(movieId);
        }

        public bool MovieEntitySetExist()
        {
            return _movieRepository.MovieEntitySetExist();
        }

        public async Task<bool> MovieExists(int movieId)
        {
            return await _movieRepository.MovieExists(movieId);
        }
    }
}
