﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Movie_Characters_API.Model.Domain;
using Movie_Characters_API.Model.DTOs.Character;
using Movie_Characters_API.Model.DTOs.Franchise;
using Movie_Characters_API.Model.DTOs.MovieFranchise;
using Movie_Characters_API.Repositories.Interfaces;
using Movie_Characters_API.Services.Interfaces;

namespace Movie_Characters_API.Services
{
    public class FranchiseService : IFranchiseService
    {
        private readonly IFranchiseRepository _franchiseRepository;
        private readonly IMovieRepository _movieRepository;
        private readonly IMapper _mapper;
      
        public FranchiseService(IFranchiseRepository franchiseRepository, IMovieRepository movieRepository, IMapper mapper)
        {
            _franchiseRepository = franchiseRepository;
            _movieRepository = movieRepository;
            _mapper = mapper;
        }

        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetAllFranchises()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _franchiseRepository.GetAllFranchises());
        }

        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int franchiseId)
        {
           return _mapper.Map<FranchiseReadDTO>(await _franchiseRepository.GetFranchise(franchiseId));
        }

        public async Task<MovieFranchiseReadDTO> GetAllMoviesInFranchise(int movieId)
        {
            return _mapper.Map<MovieFranchiseReadDTO>( await _franchiseRepository.GetAllMoviesInFranchise(movieId));
        }

        public async Task<bool> PutMoviesInFranchise(int franchiseId, int[] movieIds) 
        {
            Franchise franchiseToUpdate = await _franchiseRepository.GetFranchise(franchiseId);
                
              
            List<Movie> movies = new();
            foreach (int movieId in movieIds)
            {
                Movie? domainMovie = await _movieRepository.GetMovie(movieId);

                if (domainMovie == null)
                    throw new KeyNotFoundException();

                movies.Add(domainMovie);
            }

            franchiseToUpdate.Movies = movies;
            var didSave = await _movieRepository.SaveChanges();

            if (didSave)
                return true;

            return false;

        }

        public async Task<Franchise> CreateFranchise(FranchiseCreateDTO franchiseCreateDTO)
        {
            var franchise = _mapper.Map<Franchise>(franchiseCreateDTO);
            return await _franchiseRepository.CreateFranchise(franchise);
        }

        public async Task<Franchise> DeleteFranchises(int franchiseId)
        {
            return await _franchiseRepository.DeleteFranchise(franchiseId);
        }

        public bool FranchiseEntitySetExist()
        {
           return _franchiseRepository.FranchiseEntitySetExist();
        }

        public async Task<bool> FranchiseExist(int franchiseId)
        {
            return await _franchiseRepository.FranchiseExist(franchiseId);
        }

        public async Task<bool> UpdateFranchise(int franchiseId, FranchiseEditDTO franchiseEditDTO)
        {
            var franchise = await _franchiseRepository.GetFranchise(franchiseId);
            _mapper.Map(franchiseEditDTO, franchise);
            return await _franchiseRepository.UpdateFranchise(franchise);
        }

        public async Task<ICollection<CharacterReadDTO>> GetCharactersInFranchise(int franchiseId)
        {
            List<Character> characterList = await _franchiseRepository.GetCharactersInFranchise(franchiseId);
            List<CharacterReadDTO> characterReadDTOs = new List<CharacterReadDTO>();
            foreach (var character in characterList)
            {
                characterReadDTOs.Add(_mapper.Map<CharacterReadDTO>(character));
            }
            return characterReadDTOs;
        }
    }
}
