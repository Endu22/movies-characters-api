﻿using Microsoft.AspNetCore.Mvc;
using Movie_Characters_API.Model.Domain;
using Movie_Characters_API.Model.DTOs.Character;
using Movie_Characters_API.Model.DTOs.Franchise;
using Movie_Characters_API.Model.DTOs.MovieFranchise;

namespace Movie_Characters_API.Services.Interfaces
{
    public interface IFranchiseService
    {
        /// <summary>
        /// Get all franchises from storage.
        /// </summary>
        /// <returns>An IEnumerable of FranchiseReadDTO's.</returns>
        public Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetAllFranchises();

        /// <summary>
        /// Get a Franchise with given <paramref name="franchiseId"/> from storage.
        /// </summary>
        /// <param name="franchiseId">The id of a Franchise.</param>
        /// <returns>A FranchiseReadDTO.</returns>
        public Task<ActionResult<FranchiseReadDTO>> GetFranchise(int franchiseId);

        /// <summary>
        /// Get all the movies in a franchise from storage.
        /// </summary>
        /// <param name="franchiseId">The id of a franchise.</param>
        /// <returns>A MovieFranchiseReadDTO.</returns>
        public Task<MovieFranchiseReadDTO> GetAllMoviesInFranchise(int franchiseId);

        /// <summary>
        /// Get all characters in a franchise with given <paramref name="franchiseId"/> from storage.
        /// </summary>
        /// <param name="franchiseId">The id of a franchise.</param>
        /// <returns>A CharacterReadDTO.</returns>
        public Task<ICollection<CharacterReadDTO>> GetCharactersInFranchise(int franchiseId);

        /// <summary>
        /// Update a franchise in storage with given <paramref name="franchiseId"/> and <paramref name="franchiseEditDTO"/> data in storage.
        /// </summary>
        /// <param name="franchiseId">The id of a franchise.</param>
        /// <param name="franchiseEditDTO">The data to insert into a franchise.</param>
        /// <returns>True if successful. Else false.</returns>
        public Task<bool> UpdateFranchise(int franchiseId, FranchiseEditDTO franchiseEditDTO);

        /// <summary>
        /// Add movies to a franchise with given <paramref name="franchiseId"/> and <paramref name="movieIds"/> in storage.
        /// </summary>
        /// <param name="franchiseId">The id of the franchise to update.</param>
        /// <param name="movieIds">The ids of movies to insert.</param>
        /// <returns>True if succesful. Else false.</returns>
        public Task<bool> PutMoviesInFranchise(int franchiseId, int[] movieIds);

        /// <summary>
        /// Create a franchise containing the data in <paramref name="franchiseCreateDTO"/> and add it to storage.
        /// </summary>
        /// <param name="franchiseCreateDTO">The data to be inserted in new object.</param>
        /// <returns>The franchise created.</returns>
        public Task<Franchise> CreateFranchise(FranchiseCreateDTO franchiseCreateDTO);

        /// <summary>
        /// Delete a franchise with given <paramref name="franchiseId"/> from storage.
        /// </summary>
        /// <param name="franchiseId">The id of the franchise to delete.</param>
        /// <returns>The deleted franchise.</returns>
        public Task<Franchise> DeleteFranchises(int franchiseId);

        /// <summary>
        /// Check if a franchise with the given <paramref name="franchiseId"/> exists in storage.
        /// </summary>
        /// <param name="franchiseId">The id of a franchise.</param>
        /// <returns>True if franchise exists. Else false.</returns>
        public Task<bool> FranchiseExist(int franchiseId);

        /// <summary>
        /// Check if the storage has an entity set for franchises.
        /// </summary>
        /// <returns>True if storage contains an entity for franchises. Else false.</returns>
        public bool FranchiseEntitySetExist();
    }
}
