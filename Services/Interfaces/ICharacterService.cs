﻿using Microsoft.AspNetCore.Mvc;
using Movie_Characters_API.Model.Domain;
using Movie_Characters_API.Model.DTOs.Character;

namespace Movie_Characters_API.Services.Interfaces
{
    public interface ICharacterService
    {
        /// <summary>
        /// Get all characters from storage.
        /// </summary>
        /// <returns>An IEnumerable of CharacterReadDTO's.</returns>
        public Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetAllCharacters();

        /// <summary>
        /// Get a character with given <paramref name="characterId"/>.
        /// </summary>
        /// <param name="characterId">The id of a character.</param>
        /// <returns>A CharacterReadDTO</returns>
        public Task<ActionResult<CharacterReadDTO>> GetCharacter(int characterId);

        /// <summary>
        /// Update a character with given <paramref name="characterId"/> and the <paramref name="dtoCharacter"/> data in storage.
        /// </summary>
        /// <param name="characterId">The id if a character.</param>
        /// <param name="dtoCharacter">The data to insert into existing character.</param>
        /// <returns>True if successful update. Else false.</returns>
        public Task<bool> UpdateCharacter(int characterId, CharacterEditDTO dtoCharacter);

        /// <summary>
        /// Create a character with the given data in <paramref name="characterCreateDTO"/> and add it to storage.
        /// </summary>
        /// <param name="characterCreateDTO">The data to insert in new Character.</param>
        /// <returns>The created Character.</returns>
        public Task<Character> CreateCharacter(CharacterCreateDTO characterCreateDTO);

        /// <summary>
        /// Delete a character with given <paramref name="characterId"/> from storage.
        /// </summary>
        /// <param name="characterId">The id of character.</param>
        /// <returns>The deleted Character object.</returns>
        public Task<Character> DeleteCharacter(int characterId);

        /// <summary>
        /// Check if a character with given <paramref name="characterId"/> exists in storage.
        /// </summary>
        /// <param name="characterId">The id of a character.</param>
        /// <returns>True if character exists. Else false.</returns>
        public Task<bool> CharacterExists(int characterId);

        /// <summary>
        /// Check if storage contains an entity set for characters.
        /// </summary>
        /// <returns>True if storage contains an entity set for characters. Else false.</returns>
        public bool CharactersEntitySetExist();
    }
}
