﻿using Microsoft.AspNetCore.Mvc;
using Movie_Characters_API.Model.Domain;
using Movie_Characters_API.Model.DTOs.CharacterMovie;
using Movie_Characters_API.Model.DTOs.Movie;

namespace Movie_Characters_API.Services.Interfaces
{
    public interface IMovieService
    {
        /// <summary>
        /// Get all movies from storage.
        /// </summary>
        /// <returns>An IEnumerable of MovieReadDTO's.</returns>
        public Task<ActionResult<IEnumerable<MovieReadDTO>>> GetAllMovies();

        /// <summary>
        /// Get a movie with given <paramref name="movieId"/> from storage.
        /// </summary>
        /// <param name="movieId">The id of a movie.</param>
        /// <returns>A MovieReadDTO.</returns>
        public Task<ActionResult<MovieReadDTO>> GetMovie(int movieId);

        /// <summary>
        /// Get all characters in the movie with <paramref name="movieId"/> from storage.
        /// </summary>
        /// <param name="movieId">The id of a movie.</param>
        /// <returns>A CharacterMovieReadDTO.</returns>
        public Task<ActionResult<CharacterMovieReadDTO>> GetAllCharactersInMovie(int movieId);

        /// <summary>
        /// Update a movie with given <paramref name="movieId"/> with the data in <paramref name="dtoMovie"/> in storage.
        /// </summary>
        /// <param name="movieId">The id of the movie to be updated.</param>
        /// <param name="dtoMovie">The data that the movie should contain after update.</param>
        /// <returns>True if update was successful. Else false.</returns>
        public Task<bool> UpdateMovie(int movieId, MovieEditDTO dtoMovie);

        /// <summary>
        /// Update characters in a movie with the given <paramref name="movieId"/> and <paramref name="characterIds"/> in storage. 
        /// </summary>
        /// <param name="movieId">The id of the movie to be updated.</param>
        /// <param name="characterIds">The Ids of all the characters that should be added to the movie.</param>
        /// <returns>True if successful. Else false.</returns>
        public Task<bool> UpdateCharactersInMovie(int movieId, int[] characterIds);

        /// <summary>
        /// Create a movie and add it to storage.
        /// </summary>
        /// <param name="movieCreateDTO">The data to insert in newly created object.</param>
        /// <returns>The created Movie object.</returns>
        public Task<Movie> CreateMovie(MovieCreateDTO movieCreateDTO);

        /// <summary>
        /// Delete a movie from storage.
        /// </summary>
        /// <param name="movieId">The id of the movie to delete.</param>
        /// <returns>The deleted movie object.</returns>
        public Task<Movie> DeleteMovie(int movieId);

        /// <summary>
        /// Check if the storage has an entity set for movies.
        /// </summary>
        /// <returns>True if an entity set exists. Else false.</returns>
        public bool MovieEntitySetExist();

        /// <summary>
        /// Check if a movie with given <paramref name="movieId"/> exists in storage.
        /// </summary>
        /// <param name="movieId">The id of a movie.</param>
        /// <returns>True if movie exists. Else false.</returns>
        public Task<bool> MovieExists(int movieId);
    }
}
