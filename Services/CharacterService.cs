﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Movie_Characters_API.Data;
using Movie_Characters_API.Model.Domain;
using Movie_Characters_API.Model.DTOs.Character;
using Movie_Characters_API.Repositories.Interfaces;
using Movie_Characters_API.Services.Interfaces;

namespace Movie_Characters_API.Services
{
    public class CharacterService : ICharacterService
    {
        private readonly ICharacterRepository _characterRepo;
        private readonly IMapper _mapper;

        public CharacterService(ICharacterRepository characterRepo, IMapper mapper)
        {
            _characterRepo = characterRepo;
            _mapper = mapper;
        }

        public async Task<Character> CreateCharacter(CharacterCreateDTO characterCreateDTO)
        {
            var character = _mapper.Map<Character>(characterCreateDTO);
            return await _characterRepo.CreateCharacter(character);
        }

        public async Task<Character> DeleteCharacter(int characterId)
        {
            return await _characterRepo.DeleteCharacter(characterId);
        }

        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetAllCharacters()
        {
            return _mapper.Map<List<CharacterReadDTO>>(await _characterRepo.GetAllCharacters());
        }

        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int characterId)
        {
            return _mapper.Map<CharacterReadDTO>(await _characterRepo.GetCharacter(characterId));

        }

        public async Task<bool> UpdateCharacter(int characterId, CharacterEditDTO dtoCharacter)
        {
            var character = await _characterRepo.GetCharacter(characterId);

            _mapper.Map(dtoCharacter, character);
            return await _characterRepo.UpdateCharacter(character);
        }

        public async Task<bool> CharacterExists(int characterId)
        {
            return await _characterRepo.CharacterExists(characterId);
        }

        public bool CharactersEntitySetExist()
        {
            return _characterRepo.CharactersEntitySetExist();
        }
    }
}
