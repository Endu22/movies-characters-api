﻿using System.ComponentModel.DataAnnotations;

namespace Movie_Characters_API.Model.Domain
{
    public class Character
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string FullName { get; set; }
        [MaxLength(50)]
        public string? Alias { get; set; }
        [MaxLength(10)]
        public string? Gender { get; set; }
        [MaxLength(300)]
        public string? PictureUrl { get; set; }
        public ICollection<Movie>? Movies { get; set; }
    }
}
