﻿using System.ComponentModel.DataAnnotations;

namespace Movie_Characters_API.Model.Domain
{
    public class Franchise
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(300)]
        public string? Description { get; set; }
        public ICollection<Movie>? Movies { get; set; }
    }
}
