﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Movie_Characters_API.Model.Domain
{
    public class Movie
    {
        public int Id { get; set; }
        [MaxLength(200)]
        public string Title { get; set; }
        [MaxLength(50)]
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        [MaxLength(50)]
        public string Director { get; set; }
        [MaxLength(300)]
        public string? PictureUrl { get; set; }
        [MaxLength(300)]
        public string? Trailer { get; set; }
        public ICollection<Character>? Characters { get; set; }
        public int? FranchiseId { get; set; }
        public Franchise? Franchise { get; set; }
    }
}
