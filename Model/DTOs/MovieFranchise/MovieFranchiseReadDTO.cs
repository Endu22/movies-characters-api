﻿namespace Movie_Characters_API.Model.DTOs.MovieFranchise
{
    public class MovieFranchiseReadDTO
    {
        public List<int> Movies { get; set; }
    }
}
