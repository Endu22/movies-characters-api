﻿namespace Movie_Characters_API.Model.DTOs.CharacterMovie
{
    public class CharacterMovieReadDTO
    {
        public List<int> Characters { get; set; }
    }
}
