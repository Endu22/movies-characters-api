﻿namespace Movie_Characters_API.Model.DTOs.Franchise
{
    public class FranchiseCreateDTO
    {
        public string Name { get; set; }
        public string? Description { get; set; }
 
    }
}
