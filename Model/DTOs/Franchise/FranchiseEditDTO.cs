﻿namespace Movie_Characters_API.Model.DTOs.Franchise
{
    public class FranchiseEditDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string? Description { get; set; }

    }
}
