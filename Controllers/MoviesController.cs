﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Movie_Characters_API.Data;
using Movie_Characters_API.Model.Domain;
using Movie_Characters_API.Model.DTOs.CharacterMovie;
using Movie_Characters_API.Model.DTOs.Movie;
using Movie_Characters_API.Model.DTOs.MovieFranchise;
using Movie_Characters_API.Services.Interfaces;

namespace Movie_Characters_API.Controllers
{
    [Route("api/v1/movies")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MoviesController : ControllerBase
    {
        private readonly IMovieService _movieService;

        public MoviesController(IMovieService movieService)
        {
            _movieService = movieService;
        }

        /// <summary>
        /// Get all movies.
        /// </summary>
        /// <returns>A list of all movies.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies() => await _movieService.GetAllMovies();

        /// <summary>
        /// Get specific movie by <paramref name="movieId"/>.
        /// </summary>
        /// <param name="movieId">The id of a movie.</param>
        /// <returns> A specific movie.</returns>
        [HttpGet("{movieId}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int movieId) => await _movieService.GetMovie(movieId);

        /// <summary>
        /// Gets all characters in a movie specified by it's <paramref name="movieId"/>.
        /// </summary>
        /// <param name="movieId">The id of a movie.</param>
        /// <returns>A DTO with characters in specified movie.</returns>
        [HttpGet("{movieId}/charactersinmovie")]
        public async Task<ActionResult<CharacterMovieReadDTO>> GetAllCharacterInMovie(int movieId)
        {
            if (!await _movieService.MovieExists(movieId))
                return NotFound();

            return await _movieService.GetAllCharactersInMovie(movieId);
        }

        /// <summary>
        /// Update a movie specified by it's id.
        /// </summary>
        /// <param name="movieId">Specific movie id.</param>
        /// <param name="movieDto">movieEditDTO</param>
        /// <returns>An Action Result.</returns>
        [HttpPut("{movieId}")]
        public async Task<IActionResult> PutMovie(int movieId, MovieEditDTO movieDto)
        {
            if (movieId != movieDto.Id)
                return BadRequest();

            if (!await _movieService.MovieExists(movieId))
                return NotFound();

            await _movieService.UpdateMovie(movieId, movieDto);

            return NoContent();
        }

        /// <summary>
        /// Adds character to a movie.
        /// </summary>
        /// <param name="movieId">Specific movie id.</param>
        /// <param name="characterIds">Array of characters id</param>
        /// <returns></returns>
        [HttpPut("add-characters/{movieId}")]
        public async Task<ActionResult> PutCharacterToMovie(int movieId, int[] characterIds)
        {
            if (!await _movieService.MovieExists(movieId))
                return NotFound();

            var didUpdate = await _movieService.UpdateCharactersInMovie(movieId, characterIds);

            if (!didUpdate)
                return BadRequest();

            return NoContent();
        }

        /// <summary>
        /// Creates a new movie to database
        /// </summary>
        /// <param name="movieCreateDto">DTO for creating a new movie.</param>
        /// <returns>Returns task with an action</returns>
        [HttpPost]
        public async Task<ActionResult<MovieCreateDTO>> PostMovie(MovieCreateDTO movieCreateDto)
        {
            if (!MovieEntitySetExists())
                return Problem("Entity set 'MoviesCharactersApiDbContext.Movies' is null.");

            var domainMovie = await _movieService.CreateMovie(movieCreateDto);

            return CreatedAtAction("GetMovie", new { id = domainMovie.Id }, domainMovie);
        }

        /// <summary>
        /// Deletes a specific movie by id.
        /// </summary>
        /// <param name="movieId">The movie id.</param>
        /// <returns>Removes a movie and returns NoContent.</returns>
        [HttpDelete("{movieId}")]
        public async Task<IActionResult> DeleteMovie(int movieId)
        {
            if (!MovieEntitySetExists())
                return Problem("Entity set 'MoviesCharactersApiDbContext.Movies' is null.");

            var movie = await _movieService.DeleteMovie(movieId);

            if (movie == null)
                return NotFound();

            return NoContent();
        }

        /// <summary>
        /// Checks if the movie entity set exist.
        /// </summary>
        /// <returns>True if the entity exists. Else false.</returns>
        private bool MovieEntitySetExists()
        {
            return _movieService.MovieEntitySetExist();
        }
    }
}
