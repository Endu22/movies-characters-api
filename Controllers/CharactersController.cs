using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Movie_Characters_API.Data;
using Movie_Characters_API.Model.Domain;
using Movie_Characters_API.Model.DTOs.Character;
using Movie_Characters_API.Model.DTOs.CharacterMovie;
using Movie_Characters_API.Services.Interfaces;

namespace Movie_Characters_API.Controllers
{
    [Route("api/v1/characters")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    public class CharactersController : ControllerBase
    {
        private readonly ICharacterService _characterService;

        public CharactersController(ICharacterService characterService)
        {
            _characterService = characterService;
        }

        /// <summary>
        /// Get all characters
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters() => await _characterService.GetAllCharacters();

        /// <summary>
        /// Get spefic character by id
        /// </summary>
        /// <param name="characterId"></param>
        /// <returns>task with specific character</returns>
        [HttpGet("{characterId}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int characterId) => await _characterService.GetCharacter(characterId);

        /// <summary>
        /// Update an character
        /// </summary>
        /// <param name="characterId">specific character id</param>
        /// <param name="dtoCharacter"></param>
        /// <returns>task with an action</returns>
        [HttpPut("{characterId}")]
        public async Task<IActionResult> PutCharacter(int characterId, CharacterEditDTO dtoCharacter)
        {
            if (characterId != dtoCharacter.Id)
                return BadRequest();

            if (!await _characterService.CharacterExists(characterId))
                return NotFound();

            await _characterService.UpdateCharacter(characterId, dtoCharacter);

            return NoContent();
        }

        /// <summary>
        /// Creates a new character to database
        /// </summary>
        /// <param name="characterCreateDTO"></param>
        /// <returns>Returns task with an action</returns>
        [HttpPost]
        public async Task<ActionResult<CharacterCreateDTO>> PostCharacter(CharacterCreateDTO characterCreateDTO)
        {
            if (!CharactersEntitySetExists())
                return Problem("Entity set 'MoviesCharactersApiDbContext.Characters' is null.");

            var character = await _characterService.CreateCharacter(characterCreateDTO);

            return CreatedAtAction(nameof(GetCharacter), new { id = character.Id }, character);

        }

        /// <summary>
        /// Deletes a specific character by id
        /// </summary>
        /// <param name="characterId"></param>
        /// <returns>Removes a character</returns>
        [HttpDelete("{characterId}")]
        public async Task<IActionResult> DeleteCharacter(int characterId)
        {
            if (!CharactersEntitySetExists())
                return Problem("Entity set 'MoviesCharactersApiDbContext.Characters' is null.");

            var character = await _characterService.DeleteCharacter(characterId);

            if (character == null)
                return NotFound();

            return NoContent();
        }

        /// <summary>
        /// Checks if the character entity set exist.
        /// </summary>
        /// <returns>Return boolean true if the entity set exists in database. Else false.</returns>
        private bool CharactersEntitySetExists()
        {
            return _characterService.CharactersEntitySetExist();
        }
    }
}
