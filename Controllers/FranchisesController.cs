﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Movie_Characters_API.Data;
using Movie_Characters_API.Model.Domain;
using Movie_Characters_API.Model.DTOs.Character;
using Movie_Characters_API.Model.DTOs.CharacterMovie;
using Movie_Characters_API.Model.DTOs.Franchise;
using Movie_Characters_API.Model.DTOs.MovieFranchise;
using Movie_Characters_API.Services.Interfaces;

namespace Movie_Characters_API.Controllers
{
    [Route("api/v1/franchises")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchisesController : ControllerBase
    {
        private readonly IFranchiseService _franchiseService;

        public FranchisesController(IFranchiseService franchiseService)
        {
            _franchiseService = franchiseService;
        }

        /// <summary>
        /// Get all franchises
        /// </summary>
        /// <returns>A list of all franchises</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises() => await _franchiseService.GetAllFranchises();

        /// <summary>
        /// Get specific franchise by <paramref name="franchiseId"/>.
        /// </summary>
        /// <param name="franchiseId">The id of franchise</param>
        /// <returns>A specific franchise</returns>
        [HttpGet("{franchiseId}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int franchiseId) => await _franchiseService.GetFranchise(franchiseId);

        /// <summary>
        /// Gets all movies in a franchise specified by it's <paramref name="franchiseId"/>.
        /// </summary>
        /// <param name="franchiseId">The id of franchise</param>
        /// <returns>A DTO with movies in specified franchise.</returns>
        [HttpGet("{franchiseId}/moviesinfranchise")]
        public async Task<ActionResult<MovieFranchiseReadDTO>> GetAllMoviesInFranchise(int franchiseId) =>  await _franchiseService.GetAllMoviesInFranchise(franchiseId);

        /// <summary>
        /// Gets all charcters in a franchise specified by it's <paramref name="franchiseId"/>.
        /// </summary>
        /// <param name="franchiseId">The id of franchise</param>
        /// <returns>A DTO with characters in specified franchise.</returns>
        [HttpGet("get-characters/{franchiseId}")]
        public async Task<ActionResult<ICollection<CharacterReadDTO>>> GetCharactersInFranchise(int franchiseId)
        {
            if (!await _franchiseService.FranchiseExist(franchiseId))
                return NotFound();

            ICollection<CharacterReadDTO> characterReadDTOs = await _franchiseService.GetCharactersInFranchise(franchiseId);

            if (characterReadDTOs.ToList().Count() == 0)
                return NotFound();
            
            return characterReadDTOs.ToList();
        }

        /// <summary>
        /// Update a franchise
        /// </summary>
        /// <param name="franchiseId"> specific franchise id</param>
        /// <param name="franchiseEditDTO">FranchiseEditDTO</param>
        /// <returns>task with an action</returns>
        [HttpPut("{franchiseId}")]
        public async Task<IActionResult> PutFranchise(int franchiseId, FranchiseEditDTO franchiseEditDTO)
        {
            if (franchiseId != franchiseEditDTO.Id)
                return BadRequest();

            if(!await _franchiseService.FranchiseExist(franchiseId))
            {
                return NotFound();
            }

            await _franchiseService.UpdateFranchise(franchiseId, franchiseEditDTO);

            return NoContent();
        }

         /// <summary>
         /// Updates a movie in franchise 
         /// </summary>
         /// <param name="franchiseId">specific franchise id</param>
         /// <param name="movieIds">array of movie id</param>
         /// <returns></returns>
        [HttpPut("update/movies/{franchiseId}")]
        public async Task<IActionResult> PutMoviesInFranchise(int franchiseId, int[] movieIds)
        {
            if (!await _franchiseService.FranchiseExist(franchiseId))
                return NotFound();
            
            var didUpdate = await _franchiseService.PutMoviesInFranchise(franchiseId, movieIds);

            if (!didUpdate)
                return BadRequest();

            return NoContent();
        }

        /// <summary>
        /// Creates a new franchise to database
        /// </summary>
        /// <param name="franchiseCreateDTO">DTO for creating a new movie</param>
        /// <returns>Returns task with an action</returns>
        [HttpPost]
        public async Task<ActionResult<FranchiseCreateDTO>> PostFranchise(FranchiseCreateDTO franchiseCreateDTO)
        {
            if (FranchiseEntitySetExist() == false)
            {
                return Problem("Entity set 'MoviesCharactersApiDbContext.Franchises'  is null.");
            }

            var franchise = await _franchiseService.CreateFranchise(franchiseCreateDTO);
     
            return CreatedAtAction(nameof(GetFranchise), new { id = franchise.Id }, franchise);
        }

        /// <summary>
        /// Deletes a specific franchise with id.
        /// </summary>
        /// <param name="franchiseId">The franchise id. </param>
        /// <returns>Removes a franchise and retunrns NoContect</returns>
        [HttpDelete("{franchiseId}")]
        public async Task<IActionResult> DeleteFranchise(int franchiseId)
        {
            if (FranchiseEntitySetExist() == false)
            {
                return Problem("Entity set 'MoviesCharactersApiDbContext.Franchise' is null.");
            }
            var franchise = await _franchiseService.DeleteFranchises(franchiseId);
            if (franchise == null)
            {
                return NotFound();
            }

            return NoContent();
        }

        /// <summary>
        /// Checks if the franchise entity set exist.
        /// </summary>
        /// <returns>True if the entity exists. Else false.</returns>
        private bool FranchiseEntitySetExist()
        {
            return _franchiseService.FranchiseEntitySetExist();
        }
    }
}
