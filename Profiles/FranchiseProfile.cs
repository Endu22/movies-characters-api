﻿using AutoMapper;
using Movie_Characters_API.Model.Domain;
using Movie_Characters_API.Model.DTOs.Franchise;
using Movie_Characters_API.Model.DTOs.MovieFranchise;

namespace Movie_Characters_API.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            //Franchise --> FranchiseReadDTO
            CreateMap<Franchise, FranchiseReadDTO>()
                .ForMember(frdto => frdto.Movies, opt => opt
                .MapFrom(f => f.Movies.Select(f => f.Id).ToList()));

            //Franchise --> FranchiseCreateDTO
            CreateMap<Franchise, FranchiseCreateDTO>()
                .ReverseMap();

            //Franchise --> FranchiseEditDTO
            CreateMap<Franchise, FranchiseEditDTO>()
                .ReverseMap();
            //Franchise --> MovieFranchiseReadDTO
            CreateMap<Franchise, MovieFranchiseReadDTO>()
               .ForMember(mdto => mdto.Movies, opt => opt
                   .MapFrom(m => m.Movies.Select(m => m.Id).ToList())).ReverseMap();

        }
    }
}
