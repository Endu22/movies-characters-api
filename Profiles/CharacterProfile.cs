﻿using AutoMapper;
using Movie_Characters_API.Model.Domain;
using Movie_Characters_API.Model.DTOs.Character;


namespace Movie_Characters_API.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {

            CreateMap<Character, CharacterReadDTO>()
                      .ForMember(cdto => cdto.Movies, opt => opt
                      .MapFrom(c => c.Movies.Select(m => m.Id).ToList()))
                      .ReverseMap();

            CreateMap<Character, CharacterEditDTO>().ReverseMap();
            CreateMap<Character, CharacterCreateDTO>().ReverseMap();

        

        }
    }
}
