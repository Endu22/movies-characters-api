﻿using AutoMapper;
using Movie_Characters_API.Model.Domain;
using Movie_Characters_API.Model.DTOs.CharacterMovie;
using Movie_Characters_API.Model.DTOs.Movie;

namespace Movie_Characters_API.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            // Movie <-> MovieReadDTO
            CreateMap<Movie, MovieReadDTO>()
                .ForMember(mdto => mdto.Characters, opt => opt
                .MapFrom(m => m.Characters.Select(c => c.Id).ToArray()))
                .ForMember(mdto => mdto.Franchise, opt => opt
                .MapFrom(m => m.FranchiseId))
                .ReverseMap();

            // Movie <-> MovieEditDTO
            CreateMap<MovieEditDTO, Movie>()
                .ReverseMap();

            // Movie <-> MovieCreateDTO
            CreateMap<Movie, MovieCreateDTO>()
                .ReverseMap();

            //Movie --> CharacterMovieReadDTO
            CreateMap<Movie, CharacterMovieReadDTO>()
                .ForMember(mdto => mdto.Characters, opt => opt
                .MapFrom(m => m.Characters.Select(c => c.Id).ToList()));
        }
    }
}
