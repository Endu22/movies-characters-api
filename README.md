# Assigment Create a Web API and document it

Create an Entity Framework Code First workflow and an ASP.NET Core Web API in C# 
The assigment is completed in groups of 2.

## Acknowledgments

This project was created as a solution during the Experis Bootcamp (2022) with Noroff Education AS.

## Description

Assigment main purpose was to use Entity Framework Code First approch to create a database. Our purpose is to construct
the application in ASP.NET Core and comprise of a database made in SQL Server through EF Core
with a RESTful API to allow users to manipulate the data. The database will store information about characters, movies
they appear in and the franchises these movies belong to.

In addition to main purpose the following requirments were neccessery to do:
 - Full Crud for Movies, Characters and Franchises
 - DTOs with AutoMapper: Make data transfer objects (DTOs) for all the model classes clients will interact
 - Documentation with Swagger
 - Make us of Services/Repositories to clean up the code.

## Getting Started

To run the project it's neccessery to have, a IDE as Visual Studio and a SQL Server Management Studio. 
 
### Dependencies

* Visual Studio 2022
* Microsoft SQL Server Managment Studio 18
* Windows 10
* .NET 6 
    - ASP.NET Core Web Api
* Nuget Packet Manager
    - AutoMapper.Extensions.Microsoft.DependencyInjection 11.0.0
    - Microsoft.EntityFrameworkCore.SqlServer 6.0.8
    - Microsoft.EntityFrameworkCore.Tools 6.0.8
    - Microsoft.VisualStudio.Web.CodeGeneration.Design 6.0.8
    - Swashbuckle.AspNetCore 6.2.3
* C# 
* Swagger/Open API

### Installing

* How/where to download your program
    - git clone https://gitlab.com/Endu22/movies-characters-api.git

### Executing program

* How to run the application
    - Clone repository and open with Visual Studio.
    - In the appsettins.json file, "ConnectionStrings" --> "DefaultConnection" change the DataSource too your own server name and Initial Catalog to your local database.


## Authors

* Oliver Rimmi [@Endu22]  
    - oliver.rimmi@se.experis.com
* Elmin Nurkic [@gfunkmaster]
    - elmin.nurkic@se.experis.com

## Version History

* v1
    * Initial Release

