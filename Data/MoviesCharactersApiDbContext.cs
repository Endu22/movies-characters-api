﻿using Microsoft.EntityFrameworkCore;
using Movie_Characters_API.Model.Domain;
using System.Diagnostics.CodeAnalysis;

namespace Movie_Characters_API.Data
{
    public class MoviesCharactersApiDbContext : DbContext
    {
        public DbSet<Character> Characters => Set<Character>();
        public DbSet<Movie> Movies => Set<Movie>();
        public DbSet<Franchise> Franchises => Set<Franchise>();

        public MoviesCharactersApiDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {
        }
            
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Seed Data
            SeedingDataHelper.Seed(modelBuilder);
        }
    }
}
