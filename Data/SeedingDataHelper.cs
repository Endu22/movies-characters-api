﻿using Microsoft.EntityFrameworkCore;
using Movie_Characters_API.Model.Domain;

namespace Movie_Characters_API.Data
{
    public class SeedingDataHelper
    {
        public static void Seed(ModelBuilder modelBuilder)
        {
            //Create Characters
            Character ironMan = new()
            {
                Id = 1,
                FullName = "Tony Stark",
                Alias = "Iron Man",
                Gender = "Male",
                PictureUrl = "https://images.ecestaticos.com/FKMH-1s1qVdHOcOT0bHcBe15MPI=/0x0:991x1331/992x1332/filters:fill(white):format(jpg)/f.elconfidencial.com%2Foriginal%2F573%2Fdce%2F820%2F573dce820d6309bc592637acc464e6df.jpg"
            };

            Character jarvis = new()
            {
                Id = 2,
                FullName = "J.A.R.V.I.S",
                Alias = "Jarvis",
                Gender = null,
                PictureUrl = null
            };

            Character thor = new()
            {
                Id = 3,
                FullName = "Thor",
                Alias = "Thor",
                Gender = "Male",
                PictureUrl = "http://images4.fanpop.com/image/photos/22100000/Thor-pics-thor-2011-22155395-1707-2560.jpg"
            };

            Character jamesBond = new()
            {
                Id = 4,
                FullName = "James Bond",
                Alias = "007",
                Gender = "Male",
                PictureUrl = "https://sm.ign.com/t/ign_ap/video/n/no-time-to/no-time-to-die-teaser_ytct.1200.jpg"
            };

            // Create Franchises
            Franchise marvel = new()
            {
                Id = 1,
                Name = "Marvel",
                Description = "The Marvel Cinematic Universe is an American media franchise and " +
                              "shared universe centered on a series of superhero films produced by Marvel Studios. " +
                              "The films are based on characters that appear in American comic books published by Marvel Comics."
            };

            Franchise jamesBondFranchise = new()
            {
                Id = 2,
                Name = "007",
                Description = "James Bond is a fictional character created by British novelist Ian Fleming in 1953. " +
                              "A British secret agent working for MI6 under the codename 007."
            };


            // Create Movies
            Movie ironManMovie = new()
            {
                Id = 1,
                Title = "Iron Man",
                Genre = "Action, Adventure, Sci-Fi",
                ReleaseYear = 2008,
                Director = "Jon Favreau",
                PictureUrl = "https://upload.wikimedia.org/wikipedia/en/4/47/Iron_Man_%28circa_2018%29.png",
                Trailer = "https://www.imdb.com/video/vi447873305/?playlistId=tt0371746&ref_=tt_pr_ov_vi",
                FranchiseId = marvel.Id
            };

            Movie thorMovie = new()
            {
                Id = 2,
                Title = "Thor",
                Genre = "Action, Adventure, Fantasy",
                ReleaseYear = 2011,
                Director = "Kenneth Branagh",
                PictureUrl = "https://upload.wikimedia.org/wikipedia/en/9/95/Thor_%28film%29_poster.jpg",
                Trailer = "https://www.imdb.com/video/vi1431476761/?playlistId=tt0800369&ref_=tt_ov_vi",
                FranchiseId = marvel.Id
            };

            Movie jamesBondMovie = new()
            {
                Id = 3,
                Title = "No Time to Die",
                Genre = "Action, Adventure, Thriller",
                ReleaseYear = 2021,
                Director = "Cary Joji Fukunga",
                PictureUrl = "https://upload.wikimedia.org/wikipedia/en/f/fe/No_Time_to_Die_poster.jpg",
                Trailer = "https://www.imdb.com/video/vi3903767321/?playlistId=tt2382320&ref_=tt_ov_vi",
                FranchiseId = jamesBondFranchise.Id
            };

        
            // Seed Characters
            modelBuilder.Entity<Character>().HasData(ironMan);
            modelBuilder.Entity<Character>().HasData(jarvis);
            modelBuilder.Entity<Character>().HasData(thor);
            modelBuilder.Entity<Character>().HasData(jamesBond);
            // Seed Franchises
            modelBuilder.Entity<Franchise>().HasData(marvel);
            modelBuilder.Entity<Franchise>().HasData(jamesBondFranchise);

            // Seed movies
            modelBuilder.Entity<Movie>().HasData(ironManMovie);
            modelBuilder.Entity<Movie>().HasData(thorMovie);
            modelBuilder.Entity<Movie>().HasData(jamesBondMovie);

            // Seed linking Table Character - Movie. Many to many 
            modelBuilder.Entity<Character>()
              .HasMany(m => m.Movies)
               .WithMany(c => c.Characters)
               .UsingEntity<Dictionary<string, object>>(
                   "CharacterMovie",
                   r => r
                   .HasOne<Movie>()
                   .WithMany()
                   .HasForeignKey("MovieId"),
                   l => l
                   .HasOne<Character>()
                   .WithMany()
                   .HasForeignKey("CharacterId"),
                   je =>
                   {
                       je.HasKey("MovieId", "CharacterId");
                       je.HasData(
                           new { CharacterId = ironMan.Id, MovieId = ironManMovie.Id },
                           new { CharacterId = jarvis.Id, MovieId = ironManMovie.Id },
                           new { CharacterId = thor.Id, MovieId = thorMovie.Id },
                           new { CharacterId = jamesBond.Id, MovieId = jamesBondMovie.Id }
                       );
                   });
        }
    }
}
